#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qtxlsx/xlsx/xlsxdocument.h"
#include "smtpdialog.h"

#include <QApplication>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setTrayIcon();
    readConfig();

    TimerWork = 0;
    QTimer::singleShot(50,this,&hide);
    connect(&app_timer,SIGNAL(timeout()),this,SLOT(getFileListFromDirectory()));
    app_timer.start(10000);
}

MainWindow::~MainWindow()
{
    delete trayIconMenu;
    delete trayIcon;
    delete ui;
}

bool MainWindow::ProcessingFile(QString filename)
{
    QFile file(fileDir+filename);
    if(!file.open(QIODevice::ReadOnly)){
        //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Open File " + filename + " - error \n";
        streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Open File " + filename + " - error \r").toUtf8());
        return false;
    }
    //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Open File " + filename + " - OK\n";
    streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Open File " + filename + " - OK\r").toUtf8());
    QByteArray data = file.readAll();
    file.close();
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
    QString string = codec->toUnicode(data);
    QStringList lines = string.split(QString("\r\n"));
    for(int i = 0; i < 4; i++){
        lines.removeLast();
    }
    int rem = 0;
    for(int i = 0; i < 10; i++){
        QStringList list_lines = lines.at(i).split(";");
        if(list_lines.at(0) == "№"){
            break;
        }
        else {
           rem++;
        }
    }
    for(int i = 0; i < rem; i++){
        lines.removeFirst();
    }
    QString str = lines.at(0);
    QStandardItemModel *model = new QStandardItemModel();
    QStringList columnLines = str.split(";");
    model->setColumnCount(columnLines.length());
    for(int col = 0; col < columnLines.length();col++){
        model->setHeaderData(col,Qt::Horizontal,columnLines.at(col));
    }
    QList<QStandardItem*>listitems;
    for(int row = 0; row < lines.length()-1; row++){
        str = lines.at(row+1);
        str = str.replace("_","");
        columnLines = str.split(";");
        for(int col = 0; col < columnLines.length();col++){
            listitems.append(new QStandardItem(columnLines.at(col)));
        }
        model->insertRow(row,listitems);
        listitems.clear();
    }


    bool saveFile = SaveNewFile(model,filename);

// noreply@viveya.khv.ru
// a8sBEEeZYq
    QString nameFile = fileDir + "reestr//";
    nameFile = "\\" + nameFile.replace("//","\\");
    QString username = "noreply@viveya.khv.ru";
    QString password = "a8sBEEeZYq";
    QString server = "mail.viveya.khv.ru";
    int port = 587;
    if(MailTo.isEmpty()){
        MailTo = "SlobodchikovDA@viveya.khv.ru";
    }
    QString subject = "ПРОФОСМОТРЫ qMS ";
    QString message = "Сформирован файл реестра для Игоря по Профосмотрам (закрытые Эпизоды) по пути: " + nameFile;
    if(!saveFile){
        message = "Ошибка! Файл реестра не сформирован.";
    }

    smtpDialog *smtp = new smtpDialog();
    smtp->setParams(username,password,server,MailTo,subject,message,port);

    QThread *thread = new QThread;
    smtp->moveToThread(thread);

    // при запуске патока будет вызван метод отправки почты по smtp
    connect(thread,&QThread::started,smtp,&smtpDialog::sendMail);

    // при завершении передачи почты, отправщик почты передаст потоку сигнал finish() , вызвав срабатывание слота quit()
    connect(smtp,&smtpDialog::finish,thread,&QThread::quit);

    // поток пометит себя для удаления, по окончании работы потока
    connect(thread,&QThread::finished,thread,&QThread::deleteLater);

    // отправщик почты пометит себя для удаления при окончании работы потока
    connect(thread,&QThread::finished,smtp,&smtpDialog::deleteLater);

    //запускаем поток
    thread->start();


    QString namefile = fileDir + "backup//"+ QDateTime::currentDateTime().toString("dd.MM.yyyy_Hmmss.zzz") + "_" + filename;
    if(!file.rename(namefile)){
        //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Перенос файла " + namefile + " - error\n";
        streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Перенос файла " + namefile + " - error\r").toUtf8());
    }
    else{
        //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Перенос файла " + namefile + " - OK\n";
        streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Перенос файла " + namefile + " - OK\r").toUtf8());
    }
    return true;
}

bool MainWindow::SaveNewFile(QStandardItemModel *model, QString filename)
{
    //  №;Код организации; Дата вып;Пациент;Вид оплаты;Диагноз МКБ-10; Выполнил; Кол-во;Код по прайсу;Комплекс;Сумма;Плательщик;№ дог;полис ДМС;№ чека;ВРАЧ;МЕДСЕСТРА; САНИТАР, УБОРЩ;Вид пост
    enum model_reestr {nompor,kodorg,datavip,fio,vidopl,diagnoz,vipolnil,kolvo,kodprice,kodkompleks,summa,platelschik,nomdog,polis,nomcheck,vrach,medsestra,sanitar,vidpost};
    QSqlQuery query;
    query.prepare("select numberdogovor,namekontragent, codecomplex,codecomplexqms,namecomplex,code_price,code_service,to_char(stoimost,'999999990.00') stoimost from dogovor"
                  " left join dogovor_complex using(iddogovor)"
                  " left join dogovor_complex_services using(idcomplex)"
                  "  left join qmspriceusl on idprice = id"
                  " where approved notnull and changed isnull and closed isnull ");
    query.exec();

    QVariant s;
    int rowoffset = 0;
    QXlsx::Document xlsx;
    QXlsx::Format formatHeader, formatLeft,formatRigth, formatCenter, formatData;
    {
        formatHeader.setFontColor(QColor(Qt::blue));
        formatHeader.setFontSize(16);
        formatHeader.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        formatHeader.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        formatHeader.setBorderStyle(QXlsx::Format::BorderThin);
        formatHeader.setTextWarp(true);
        formatLeft.setFontColor(QColor(Qt::black));
        formatLeft.setFontSize(12);
        formatLeft.setHorizontalAlignment(QXlsx::Format::AlignLeft);
        formatLeft.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        formatLeft.setBorderStyle(QXlsx::Format::BorderThin);
        formatLeft.setTextWarp(true);

        formatRigth.setFontColor(QColor(Qt::black));
        formatRigth.setFontSize(12);
        formatRigth.setHorizontalAlignment(QXlsx::Format::AlignRight);
        formatRigth.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        formatRigth.setBorderStyle(QXlsx::Format::BorderThin);

        formatCenter.setFontColor(QColor(Qt::black));
        formatCenter.setFontSize(12);
        formatCenter.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        formatCenter.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        formatCenter.setBorderStyle(QXlsx::Format::BorderThin);
        formatCenter.setTextWarp(true);

        formatData.setFontColor(QColor(Qt::black));
        formatData.setFontSize(12);
        formatData.setHorizontalAlignment(QXlsx::Format::AlignRight);
        formatData.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        formatData.setBorderStyle(QXlsx::Format::BorderThin);
        formatData.setNumberFormat("dd.mm.yyyy");
    }
    int col = 0;
    {
        s = QString("Дата вып");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Пациент");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Кол-во");
        xlsx.setColumnWidth(col+1,15);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Код по прайсу");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Сумма");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("№ дог");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("ВРАЧ");
        xlsx.setColumnWidth(col+1,50);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("МЕДСЕСТРА");
        xlsx.setColumnWidth(col+1,50);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("УБОРЩИК");
        xlsx.setColumnWidth(col+1,50);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Вид пост");
        xlsx.setColumnWidth(col+1,30);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
    }
    rowoffset++;
    for(int row = 0; row < model->rowCount(); row++){
        QString dognom = model->data(model->index(row,model_reestr::nomdog)).toString();
        QString komplkod = model->data(model->index(row,model_reestr::kodkompleks)).toString();
        QString pricekod = model->data(model->index(row,model_reestr::kodprice)).toString();
        QString servicekod = "";
        QString summ = "";
        if(dognom.isEmpty() || komplkod.isEmpty()
                || pricekod.isEmpty()){
            continue;
        }
        query.first();
        bool find = false;
        do{
            if(dognom == query.value("numberdogovor").toString() && komplkod == query.value("codecomplexqms").toString() && (pricekod == query.value("code_price").toString() || pricekod == "20-34")){
                servicekod = pricekod == "20-34" ? QString("20-34") : query.value("code_service").toString();
                summ = query.value("stoimost").toString();
                find = true;
                break;
            }
        }while(query.next());
        if(!find){
           continue;
        }
        s =  model->data(model->index(row,model_reestr::datavip)).toString();
        xlsx.write(1+rowoffset,1,s,formatData);
        s =  model->data(model->index(row,model_reestr::fio)).toString();
        xlsx.write(1+rowoffset,2,s,formatLeft);
        s =  model->data(model->index(row,model_reestr::kolvo)).toString();
        xlsx.write(1+rowoffset,3,s,formatCenter);
        s =  servicekod;
        xlsx.write(1+rowoffset,4,s,formatCenter);
        s =  summ;
        xlsx.write(1+rowoffset,5,s,formatRigth);
        s =  model->data(model->index(row,model_reestr::nomdog)).toString();
        xlsx.write(1+rowoffset,6,s,formatCenter);
        s =  model->data(model->index(row,model_reestr::vrach)).toString();
        xlsx.write(1+rowoffset,7,s,formatLeft);
        s =  model->data(model->index(row,model_reestr::medsestra)).toString();
        xlsx.write(1+rowoffset,8,s,formatLeft);
        s =  model->data(model->index(row,model_reestr::sanitar)).toString();
        xlsx.write(1+rowoffset,9,s,formatLeft);
        s =  model->data(model->index(row,model_reestr::vidpost)).toString();
        xlsx.write(1+rowoffset,10,s,formatCenter);
        rowoffset++;
    }
    QString namefile = fileDir + "reestr//"+ QDateTime::currentDateTime().toString("dd.MM.yyyy_HHmmss.zzz") + "_" + filename + ".xlsx";
    if(!xlsx.saveAs(namefile)){
        //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Запись файла " + namefile + " - error\n";
        streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Запись файла " + namefile + " - error\r").toUtf8());
        return false;
    }
    //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Запись файла " + namefile + ".xlsx - OK\n";
    streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Запись файла " + namefile + ".xlsx - OK\r").toUtf8());
    return true;
}

void MainWindow::OpenLogFile()
{
    logData.clear();
    streamer.clear();
    logfile.setFileName(fileDir + "log//log.txt");
    if(logfile.exists()){
        if(logfile.size() < 200000){
            logfile.open(QIODevice::ReadOnly);
            logData = logfile.readAll();
            logfile.close();
        }
    }
    logfile.open(QIODevice::WriteOnly | QFile::Text);
    //streamer.setDevice(&logfile);
    //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss") + " Open Log file\n";
    streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss") + " Open Log file\r").toUtf8());
}

void MainWindow::CloseLogFile()
{
    //streamer << "\n";
    //streamer.flush();
    if(!logData.isEmpty()){
            streamer.append(QString("\r").toUtf8());
    }
    streamer.append(logData);
    logfile.write(streamer);
    logfile.close();
}

void MainWindow::readConfig()
{
    QString filename = QApplication::applicationName() + ".ini";
    QFile inifile(filename);
    if(!inifile.exists()){
        inifile.open(QIODevice::WriteOnly | QFile::Text);
        QTextStream streamer;
        streamer.setDevice(&inifile);
        streamer << "HostName=pamir.viveya.local\n"
                    "DatabaseName=qmsreestr\n"
                    "MailTo=SlobodchikovDA@viveya.khv.ru";
        streamer.flush();
        inifile.close();
    }
    inifile.open(QIODevice::ReadOnly);
    //QByteArray configData = inifile.readAll();
    QStringList configData = QTextCodec::codecForName("Windows-1251")->toUnicode(inifile.readAll()).split("\r\n");
    inifile.close();
    QMap<QString,QString> mapConfigData;
    for(int pos = 0; pos < configData.length(); pos++){
        QString str = configData.at(pos);
        QStringList list_str = str.split('=');
        if(list_str.length()==2){
            QString key = list_str.at(0);
            QString value = list_str.at(1);
            mapConfigData.insert(key.trimmed(),value.trimmed());
        }
    }

    HostName = mapConfigData.value("HostName");
    DatabaseName = mapConfigData.value("DatabaseName");
    MailTo = mapConfigData.value("MailTo");
}

void MainWindow::setTrayIcon()
{
    // Setting system tray's icon menu...
    trayIconMenu = new QMenu(this);

    trayIconMenu->addAction("Выход",this,&close);

    // Создаём экземпляр класса и задаём его свойства...
    trayIcon = new QSystemTrayIcon(this);
    QIcon trayImage(":/icons/save256.png");
    trayIcon->setIcon(trayImage);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setToolTip(QString("%1 %2 %3 %4")
                         .arg(QCoreApplication::organizationName()+"\n")
                         .arg(QCoreApplication::organizationDomain()+"\n")
                         .arg(QCoreApplication::applicationName())
                         .arg(QCoreApplication::applicationVersion()));
    // Подключаем обработчик клика по иконке...
    //connect(&trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));

    // Выводим значок...
    trayIcon->show();
}

void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason)
{

    //QSystemTrayIcon *trayIcon = static_cast<QSystemTrayIcon*>(sender());
    //trayIcon->contextMenu()->showNormal();
    //qDebug() << reason;
}

void MainWindow::getFileListFromDirectory()
{
    TimerWork ++;
    if(TimerWork == 300){
        this->close();
    };
    QStringList filter;
    filter << "*.csv";
    QStringList filesName;
    QDir filesDir(fileDir);
    filesName = filesDir.entryList(filter);

    //qDebug() << filesName;
    if(!filesName.empty()){
        app_timer.stop();
        OpenLogFile();
        QSqlDatabase db = QSqlDatabase::database();
        if(!db.isValid())
        {
            db = QSqlDatabase::addDatabase("QPSQL");
            db.setHostName(HostName);
            db.setPort(5432);
            db.setDatabaseName(DatabaseName);
            db.setUserName("qmsreestr");
            db.setPassword("1q2w3e@1");
        }
        if(!db.isOpen()){
            if(!db.open()){
                //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Открытие БД  " + db.hostName() + ":" +QString::number(db.port()) + " " +db.databaseName() + " - error :" + db.lastError().text() ;
                streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Открытие БД  " + db.hostName() + ":" +QString::number(db.port()) + " " +db.databaseName() + " - error :" + db.lastError().text() + "\r").toUtf8()) ;
                CloseLogFile();
                app_timer.start();
                return ;
            }
            else{
                //streamer << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Открытие БД " + db.hostName() + ":" +QString::number(db.port()) + " " +db.databaseName() + " - Ок\n";
                streamer.append(QString(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss.zzz") + " Открытие БД " + db.hostName() + ":" +QString::number(db.port()) + " " +db.databaseName() + " - Ок\r").toUtf8());
            }
        }

        for(int i = 0; i < filesName.length(); i++){
            QString filename = filesName.at(i);
            ProcessingFile(filename);
        }
        CloseLogFile();
        db.close();
        QSqlDatabase::removeDatabase(db.connectionName());
        app_timer.start();
    }
}


