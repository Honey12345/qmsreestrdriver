#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QDir>
#include <QFile>
#include <QByteArray>
#include <QTextStream>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTextCodec>
#include <QSqlDatabase>
#include <QSqlError>
#include <QDateTime>
#include <QSqlQuery>
#include <QThread>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool ProcessingFile(QString filename);
    bool SaveNewFile(QStandardItemModel *model,QString filename);
    void OpenLogFile();
    void CloseLogFile();
    void readConfig();

private slots:

    void setTrayIcon();
    void trayIconActivated(QSystemTrayIcon::ActivationReason);

    void getFileListFromDirectory();

private:
    Ui::MainWindow *ui;

    QMenu *trayIconMenu;
    QSystemTrayIcon *trayIcon;
    QTimer app_timer;
    QString fileDir = "//nasa//qms_analytic_result//qMSReestr//";
    QFile logfile;
    QByteArray logData, streamer;
    //QTextStream streamer;
    QString HostName, DatabaseName, MailTo;
    int TimerWork;
};

#endif // MAINWINDOW_H
